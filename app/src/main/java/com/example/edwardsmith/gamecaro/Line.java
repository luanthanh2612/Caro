package com.example.edwardsmith.gamecaro;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by EdwardSmith on 2/18/18.
 */

public class Line {

    private Point startPoint;
    private Point endPoint;

    public Line(Point startPoint, Point endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public void draw(Canvas canvas, Paint paint){
        canvas.drawLine(startPoint.getX(),startPoint.getY(),endPoint.getX(),endPoint.getY(),paint);
    }


}
