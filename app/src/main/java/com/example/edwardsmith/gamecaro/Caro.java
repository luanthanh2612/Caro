package com.example.edwardsmith.gamecaro;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EdwardSmith on 2/18/18.
 */

public class Caro {

    private int widthBoard;
    private int heightBoard;
    private int rowNum;
    private int colNum;
    private Context context;
    private Canvas canvas;
    private Bitmap bmp;
    private Paint paint;
    private List<Line> listLine = new ArrayList<>();
    private int[][] states;
    private int player = 0;
    Bitmap bitmapCross,bitmapTick;

    public Caro(Context context,int widthBoard, int heightBoard, int rowNum, int colNum) {
        this.widthBoard = widthBoard;
        this.heightBoard = heightBoard;
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.context = context;
    }

    public void init() {


        states = new int[rowNum][colNum];
        for (int i = 0 ;i < states.length;i++){
            for (int j = 0;j < states.length;j++){
                states[i][j] = - 1;
            }
        }


        int cellWidth = widthBoard / colNum;
        int cellHeight = heightBoard / rowNum;

        //draw vertical boad
        for (int i = 0; i <= colNum; i++) {
            Line lineVertical = new Line(new Point(i * cellWidth, 0), new Point(i * cellWidth, heightBoard));
            listLine.add(lineVertical);
        }

        //draw horizontal boad
        for (int i = 0; i <= rowNum; i++) {
            Line lineHorizoltal = new Line(new Point(0, i * cellHeight), new Point(widthBoard, i * cellHeight));
            listLine.add(lineHorizoltal);
        }

        bitmapCross = BitmapFactory.decodeResource(context.getResources(),R.drawable.cross);

        bitmapTick = BitmapFactory.decodeResource(context.getResources(),R.drawable.ecard);



    }

    public Bitmap drawBoard() {
        bmp = Bitmap.createBitmap(widthBoard, heightBoard, Bitmap.Config.ARGB_8888);

        canvas = new Canvas(bmp);

        paint = new Paint();
        paint.setStrokeWidth(2.0f);
        paint.setColor(Color.RED);

        for (Line line : listLine) {
            line.draw(canvas, paint);
        }

        return bmp;

    }
    public boolean onTouch(View v, MotionEvent e){
        switch (e.getAction()){
            case MotionEvent.ACTION_DOWN:

                int x = (int) e.getX();
                int y = (int) e.getY();

                int width = v.getWidth();
                int height = v.getHeight();

                int cellWidth = width/colNum;
                int cellHeight = height/rowNum;

                int col = x/cellWidth;
                int row = y/ cellHeight;


                int cellBitMapWidth = widthBoard/colNum;
                int cellBitMapHeight = heightBoard/rowNum;

                int cellX = col * cellBitMapWidth;
                int cellY = row * cellBitMapHeight;

                Point topLeft = new Point(cellX + 5,cellY + 5);
                Point rigthBotton = new Point(cellBitMapWidth + cellX - 5,cellBitMapHeight + cellY - 5);

                if (states[row][col] != -1){
                    break;
                }


                states[row][col] = player;

                checkWin(player);

                Bitmap subBitmap;
                subBitmap = player == 0 ? bitmapCross : bitmapTick;
                canvas.drawBitmap(subBitmap,new Rect(0,0,cellBitMapWidth,cellBitMapHeight),
                        new Rect(topLeft.getX(),topLeft.getY(),rigthBotton.getX(),rigthBotton.getY()),paint);



                player = (player + 1)%2;
                v.invalidate();

                break;
        }
        return false;
    }

    private boolean checkWin(int player){

        if (player == 0){

//            for (int i = 0;i<states.length;i++){
//                Log.d("check",states[i][i] + "");
//            }

        }else if (player == 1){

        }

        return false;
    }
}
