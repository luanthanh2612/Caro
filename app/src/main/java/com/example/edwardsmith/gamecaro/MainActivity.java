package com.example.edwardsmith.gamecaro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    ImageView imgViewCaro;
    public static final int WIDTH_BOARD = 300;
    public static final int HEIGHT_BOARD = 300;
    public static final int ROW_NUM = 3;
    public static final int COL_NUM = 3;
    Caro caro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgViewCaro = findViewById(R.id.imageViewCaro);

        caro = new Caro(getApplicationContext(),WIDTH_BOARD,HEIGHT_BOARD,ROW_NUM,COL_NUM);
        caro.init();

        imgViewCaro.setImageBitmap(caro.drawBoard());
        imgViewCaro.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        return caro.onTouch(view,motionEvent);
    }
}
